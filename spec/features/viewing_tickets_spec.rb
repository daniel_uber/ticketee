require 'rails_helper'

RSpec.feature 'Users can view tickets' do
  let(:author) { FactoryGirl.create(:user) }
  before do
    sublime = FactoryGirl.create(:project, name: 'Sublime Text 3')
    FactoryGirl.create(:ticket,
                       author:      author,
                       project:     sublime,
                       name:        'Make it shiny!',
                       description: 'Gradients! Starbursts! Oh my!')
    assign_role!(author, :viewer, sublime)
    ie = FactoryGirl.create(:project, name: 'Internet Explorer')
    FactoryGirl.create(:ticket,
                       author:      author,
                       project:     ie,
                       name:        'Standards Compliance',
                       description: 'Isn\'t a joke')

    login_as(author)
    visit '/'
  end

  scenario 'for a given project' do
    click_link 'Sublime Text 3'

    expect(page).to have_content 'Make it shiny!'
    expect(page).to_not have_content 'Standards Compliance'

    click_link 'Make it shiny!'
    within('#ticket h2') do
      expect(page).to have_content 'Make it shiny!'
    end

    expect(page).to have_content 'Gradients! Starbursts! Oh my!'
  end
end
