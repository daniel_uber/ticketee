FactoryGirl.define do
  factory :ticket do
    name 'Example Ticket'
    description 'Example Description for an Example Ticket'
    author User.new
  end
end
