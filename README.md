# README

This is the example project/ticket application from Rails 4 in Action, worked through in Rails 5 with minimal differences.

Using:

* Ruby 2.3.1

* Rails 5

To run:
clone, bundle, rails db:setup (and possibly db:seed, db:migrate or db:schema:load), rails s


